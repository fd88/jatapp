export const environment = {
  production: true,
  api: 'https://gne5ydctv0.execute-api.us-east-1.amazonaws.com/prod/',
  locales: ['en'],
  defaultLocale: 'en'
};
