import {
	Component,
	ComponentFactory,
	ComponentFactoryResolver,
	ComponentRef,
	OnDestroy,
	OnInit,
	ViewChild,
	ViewContainerRef
} from '@angular/core';
import { environment } from '../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { User } from './types/user.interface';
import { interval, of, Subscription, timer } from 'rxjs';
import { UsersService } from './services/users.service';
import { mergeMap } from 'rxjs/operators';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Подписка на фоновый тайммер (10-секундный)
	 */
	private bgTimer: Subscription;

	/**
	 * Подписка на таймер запросов (не больше 4 запроса в секунду)
	 */
	private reqTimer: Subscription;

	/**
	 * Модальное окно
	 */
	private dialog: ComponentRef<ModalDialogComponent>;

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Количество выводимых строк таблицы
	 */
	readonly NUM_OF_ROWS = 20;

	/**
	 * Заголовок таблицы
	 */
	title = 'app_title';

	/**
	 * Количество строк (по-умолчанию - 3)
	 */
	numOfRows = 3;

	/**
	 * Список полученных юзеров
	 */
	users: Array<User> = [];

	/**
	 * Идет ли загрузка?
	 */
	isLoading = false;

	/**
	 * Контейнер для модального окна
	 */
	@ViewChild('dialogContainer', { read: ViewContainerRef }) container;

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
							private readonly translateService: TranslateService,
							private readonly usersService: UsersService,
							private readonly resolver: ComponentFactoryResolver
) {
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Функция получения пользователей
	 */
	private getUsers(): void {
		this.stopRequestTimer();
		this.stopBgTimer();
		this.users = [];
		this.isLoading = true;
		this.startRequestTimer();
	}

	/**
	 * Старт фонового таймера
	 */
	private startBgTimer(startDelay = 0): void {
		this.stopRequestTimer();
		this.bgTimer = timer(startDelay, 10000)
			.subscribe(() => this.getUsers());
	}

	/**
	 * Стоп фонового таймера
	 */
	private stopBgTimer(): void {
		if (this.bgTimer) {
			this.bgTimer.unsubscribe();
			this.bgTimer = null;
		}
	}

	/**
	 * Старт таймера запросов
	 */
	private startRequestTimer(perSecond = 4): void {
		if (!this.reqTimer) {
			let requestsLeft = this.numOfRows;
			this.reqTimer = interval(1000)
				.subscribe(() => {
					const requestsPerSecond = Math.min(requestsLeft, perSecond);
					if (requestsLeft <= 0) {
						this.isLoading = false;
						this.startBgTimer(10000);
					} else {
						of(...(new Array(requestsPerSecond)))
							.pipe(
								mergeMap(() => this.usersService.getOneUser()),
							)
							.subscribe(v => {
								this.users.push(v);
							}, err => console.log('Error:', err));
					}
					requestsLeft -= perSecond;
				});
		}
	}

	/**
	 * Стоп таймера запросов
	 */
	private stopRequestTimer(): void {
		if (this.reqTimer) {
			this.reqTimer.unsubscribe();
			this.reqTimer = null;
		}
	}

	/**
	 * Событие изменения количества рядов
	 */
	onRowsCountChanged(): void {
		this.getUsers();
	}

	/**
	 * Слушатель клика на строке таблицы
	 */
	onTableClickHandler($event: MouseEvent): void {
		if (!this.isLoading) {
			this.stopBgTimer();
			const tableRow = ($event.target as HTMLDivElement).closest('.table__row');
			const dataIndex = +(tableRow as HTMLDivElement).dataset['index'];
			this.container.clear();
			const factory: ComponentFactory<ModalDialogComponent> = this.resolver
				.resolveComponentFactory(ModalDialogComponent);
			this.dialog = this.container.createComponent(factory);
			this.dialog.instance.user = this.users[dataIndex];
			this.dialog.instance.closed.subscribe(() => {
				this.container.clear();
				this.startBgTimer();
			});
		}
	}

	ngOnInit(): void {
		this.translateService.use(environment.defaultLocale);
		this.startBgTimer();
	}

	ngOnDestroy(): void {
		this.stopRequestTimer();
		this.stopBgTimer();
	}
}
