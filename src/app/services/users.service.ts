import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../types/user.interface';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class UsersService {
	constructor(private readonly http: HttpClient) {	}

	getOneUser(): Observable<User> {
		return this.http.get(environment.api);
	}
}
