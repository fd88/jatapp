import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../../types/user.interface';

@Component({
	selector: 'app-modal-dialog',
	templateUrl: './modal-dialog.component.html',
	styleUrls: ['./modal-dialog.component.scss']
})
export class ModalDialogComponent implements OnInit {

	readonly DEFAULT_AVATAR = '/assets/images/avatar.jpg';
	@Input() user: User;
	@Output() readonly closed = new EventEmitter();

	onCloseHandler(): void {
		this.closed.emit(null);
	}

	ngOnInit(): void {
	}

}
